####################################################################################################
# DO NOT EXEC, ONLY SOURCING
# NOTE: POD_ROOT env var must be set
####################################################################################################
# GENERAL CONFIG

# 0) Pod
CFG_LIVE_DATA="${POD_ROOT}/live" # folder for live container
CFG_SHARE_DIR="${CFG_LIVE_DATA}/shared"

# 1) Network
# 1.1) Domain name
CFG_DOMAIN_NAME="bobcorp.net"
CFG_DOMAIN_MAILER="iredmail"
CFG_DOMAIN_DNS="dns"
CFG_DOMAIN_PROXY="nginx"
CFG_DOMAIN_EXTERNAL="$(domainname -f)"
CFG_DOMAIN_WEBSRV="wordpress"
CFG_DOMAIN_TESTSRV="jenkins"
CFG_DOMAIN_WORKSRV="ubuntu-kasm"
CFG_DOMAIN_FILESRV="nextcloud"

# 1.2) Localhost service port definition
CFG_NGINX_PORT_HTTP=8080
CFG_NGINX_PORT_HTTPS=8443

CFG_WP_PORT_HTTP=8081
CFG_ROUNDCUBE_PORT_HTTPS=8082
CFG_JENKINS_PORT_HTTP=8083
CFG_OFFICE_PORT_HTTPS=8084
CFG_UBUNTU_XPRA_PORT_HTTPS=8085
CFG_UBUNTU_KASM_PORT_HTTPS=8086
CFG_NEXTCLOUD_PORT_HTTP=8087
CFG_SF_PORT_HTTP=8088

# 2) User (for services) and password defintion
CFG_DEFAULT_USERNAME="postmaster"
CFG_DEFAULT_PASSWORD="pass"

# 3) Online Server Access (for clone.sh)
CFG_SRV_FTP_HOST="<ftp_server_name_to_config>"
CFG_SRV_FTP_USER="<ftp_server_username_to_config>"
CFG_SRV_FTP_PASS="<ftp_server_password_to_config>"
CFG_SRV_DB_HOST="<database_host_to_config>"
CFG_SRV_DB_NAME="<database_name_to_config>"
CFG_SRV_DB_USER="<database_username_to_config>"
CFG_SRV_DB_PASS="<database_password_to_config>"

####################################################################################################
# DO NOT EDIT OVER THERE - INTERNAL CONFIG
for _c in $(ls $POD_ROOT/*-config.sh); do source $_c; done

#TODO: update the name to primary_db (to be general for all apps)
CFG_DB_NAME="wordpress_db"
CFG_DB_USER=$CFG_DEFAULT_USERNAME
CFG_DB_PASS=$CFG_DEFAULT_PASSWORD

CFG_ROOTFS=".initrootfs"
CFG_EXTRA_CONFIG_START_FLAG="EXTRA_CONFIG_HEADER"
CFG_EXTRA_CONFIG_END_FLAG="EXTRA_CONFIG_FOOTER"

