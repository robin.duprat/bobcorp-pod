####################################################################################################
# CORE UTILS
PID_LIST=()

print_error() {
    local prev_ret_value=$1; shift
    local err_msg=$*
    if [[ $prev_ret_value -ne 0 ]]; then
        error $err_msg
    fi

    return $prev_ret_value
}

print_error_exit() {
    local prev_ret_value=$1; shift
    local err_msg=$*
    print_error $prev_ret_value $err_msg || exit 1
}

podman_restart() {
    _container=$1
    _is_forced=$2
    if [[ -n $_is_forced ]] || [[ "$(podman container inspect -f '{{.State.Running}}' $_container)" != "true" ]]; then
        podman container restart $_container &>> $DEBUG_LOG || podman container restart $_container &>> $DEBUG_LOG
        silent "\t\t$1 container have been restart"
    fi
}

podman_getlist() {
    podman ps --format "{{.Names}}"
}

podman_getip() {
    _container=$1
    if [[ "$(podman container inspect -f '{{.State.Running}}' $_container)" == "true" ]]; then
        echo $(podman inspect $_container -f "{{ .NetworkSettings.Networks.$NETWORK_NAME.IPAddress }}")
    fi
}

add_local_setup_cmd() {
    _container=$1
    _extra_setup_cmd=$2
    _extra_setup_success=${3:-"$2"}
    _extra_setup_restart=${4}
    _extra_setup_name=${5:-"$RANDOM"}
    _tmp_file="/tmp/.${_container}_localsetup-$_extra_setup_name"
    ( _pid=$(while ! test -f $_tmp_file.pid; do continue; done; cat $_tmp_file.pid); rm $_tmp_file.pid
      if bash -c "$_extra_setup_cmd" &> $_tmp_file.log; then
          cat $_tmp_file.log &>> $DEBUG_LOG; rm $_tmp_file.log
          notify "Extra setup for ${_container}($_pid) : $_extra_setup_success"
          [[ -n $_extra_setup_restart ]] && podman_restart $_extra_setup_restart
      else
          print_error $? "local cmd ${_container}($_pid) failed - Skip"
      fi
    ) & PID_LIST+=($!)
    echo $! > $_tmp_file.pid
}

add_setup_cmd_to_ctn() {
    _container=$1
    _extra_setup_cmd=$2
    _extra_setup_success=${3:-"$2"}
    _extra_setup_restart=${4}
    _extra_setup_name=${5:-"$RANDOM"}
    _tmp_file="/tmp/.${_container}_remotesetup-$_extra_setup_name"
    ( _pid=$(while ! test -f $_tmp_file.pid; do continue; done; cat $_tmp_file.pid); rm $_tmp_file.pid
      if podman container exec -u 0 $_container /bin/bash -c "$_extra_setup_cmd" &> $_tmp_file.log; then
          cat $_tmp_file.log &>> $DEBUG_LOG; rm $_tmp_file.log
          notify "Extra setup for ${_container}($_pid) : $_extra_setup_success"
          [[ -n $_extra_setup_restart ]] && podman_restart $_extra_setup_restart
      else
          print_error $? "remote cmd ${_container}($_pid) failed - Skip"
      fi
    ) & PID_LIST+=($!)
    echo $! > $_tmp_file.pid
}

add_pkg_to_ctn() {
    _container=$1
    _extra_pkg_list=$2
    _extra_setup_restart=${3}
    _extra_setup_cmd="apt-get update && apt-get install -y $_extra_pkg_list"
    add_setup_cmd_to_ctn $_container "$_extra_setup_cmd" "'$_extra_pkg_list' added" "$_extra_setup_restart"
}

get_podcfg() {
    name=$1
    variable=$2
    if ! [[ $name =~ / ]]; then
        pod_name=$(dirname $(find ${INFRA_ROOT} -wholename "*_$name/pod.env"))
        pod_name=${pod_name#"${INFRA_ROOT}"}
    else
        pod_name=$name
    fi

   (. $INFRA_ROOT/$pod_name/pod.cfg; eval echo \$$variable)
}


