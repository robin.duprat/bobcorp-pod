#!/bin/bash -i
####################################################################################################
# Note: Use interactive to get bashrc loaded, and so info/debug/error logger functions too.
#       if logger functions are not loaded, wrap them to avoid meaningless errors
for fct in silent notify info warn error success; do
    if ! [[ $(type -t $fct) =~ "function" ]] &>/dev/null; then eval "$fct()" '{ echo ---- $*; }'; fi
done

####################################################################################################
# 0) Log debug setup
DEBUG_LOG="/dev/null" # TO DISABLE LOGS, COMMENT NEXT LINE
DEBUG_LOG=.restore.log
SRC_ARCHIVE=$1
[[ $DEBUG_LOG != "/dev/null" ]] && rm -f $DEBUG_LOG && exec &> >(tee -a "$DEBUG_LOG" | sed '/^\+/d' )
info ">>>>>  ENTRY-POINT  <<<<<"
silent "USER:   $USER"
silent "HOST:   $(domainname -f)"
silent "root:   $([[ $UID == 0 ]] && echo 'yes' || echo 'no')"
silent "PID:    $$"
silent "PPID:   $PPID"
silent "PWD:    $PWD"
silent "SOURCE: $SRC_ARCHIVE"

####################################################################################################
# 1) Setup environment
source config.sh
# 1.1) create main working directory
info "Init backup restoration"
ROOTFS="$CFG_ROOTFS"
sudo rm -rf $ROOTFS && mkdir -p $ROOTFS
CMD_ZIP="unzip $SRC_ARCHIVE -d $ROOTFS"
CMD_TAR="tar --same-owner -xf $SRC_ARCHIVE -C $ROOTFS"
# DEPRECATED CMD=$CMD_2IP
CMD=$CMD_TAR
(sudo $CMD) &>> $DEBUG_LOG && \
success "Backup successfully extracted" || \
warn "Backup not correctly extracted."

