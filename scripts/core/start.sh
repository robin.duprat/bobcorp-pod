#!/bin/bash -i
####################################################################################################
# START
# Note: Use interactive to get bashrc loaded, and so info/debug/error logger functions too.
#       if logger functions are not loaded, wrap them to avoid meaningless errors. Disable History.
set +o history
# Use the logger functions if defined, else substitute them
for fct in silent notify info warn error success; do
    if ! [[ $(type -t $fct) =~ "function" ]] &>/dev/null; then eval "$fct()" '{ echo ---- $*; }'; fi
done


####################################################################################################
# INIT_1: LOCAL FUNCTIONS

is_circular_dep() {
    # Use this to get array from args (using name ref, bash4.3 or higher)
    local -n run_list=$1
    local -n dep_array=$2
    is_circular_dep=false
    for require in ${!dep_array[*]}; do
        start_elt=$require
        elt=$require
        is_stop=false
        while ! $is_stop; do
            for _r in ${!dep_array[*]}; do
                if [[ ${dep_array[$_r]} =~ $elt ]]; then
                    elt=$_r
                    if [[ $elt == $start_elt ]]; then
                        is_circular_dep=true
                        is_stop=true
                    fi
                fi
            done
            is_stop=true
        done
    done

    eval $is_circular_dep
}

get_dep_run_list() {
    # Use this to get array from args (using name ref, bash4.3 or higher)
    run_list="$*"
    new_run_list=""
    elt_list=""
    nb_run=0

    while [[ "$new_run_list" != "$run_list" ]]; do
        nb_run=$(($nb_run + 1))
        declare -A DEPENDENCY_ARRAY
        for container in $run_list; do
            c_name=$(get_podcfg $container CTN_NAME)
            c_dep=$(get_podcfg $container CTN_DEPENDENCY)
            for dep in $c_dep; do
                if ! [[ ${DEPENDENCY_ARRAY[$c_name]} =~ $dep ]]; then
                    DEPENDENCY_ARRAY[$c_name]+=" $dep"
                fi
            done
        done

        if is_circular_dep DEPENDENCY_ARRAY run_list; then
            for require in ${!DEPENDENCY_ARRAY[*]}; do
                silent "\t\t$(echo ${DEPENDENCY_ARRAY[$require]}) needs $require"
            done
            print_error_exit 1 "Circular dependency found ! - Abort"
        fi

        new_run_list="$run_list"
        for run_elt in $run_list; do
            is_stop=false
                        c_name=$(get_podcfg $run_elt CTN_NAME)
            if [[ -z $(get_podcfg $run_elt CTN_DEPENDENCY) ]]; then
                if ! [[ "$elt_list" =~ $c_name ]]; then
                    elt_list="$c_name $elt_list"
                fi
            else
                for elt in $(get_podcfg $run_elt CTN_DEPENDENCY); do
                    if ! [[ "$elt_list" =~ $elt ]]; then
                        elt_list="$(get_podcfg $elt CTN_NAME) $elt_list"
                    fi
                done
                if ! [[ "$elt_list" =~ $c_name ]]; then
                    elt_list+=" $c_name"
                fi
            fi
            # Note: may be needed.. but no use case to check it
            #while ! $is_stop; do
            #    is_stop=true
            #    elt=${elt_list%% *}
            #    if [[ -n $elt ]]; then
            #        new_list=""
            #        for new_dep in $(get_podcfg $elt CTN_DEPENDENCY); do
            #            if ! [[ $elt_list =~ $new_dep ]]; then
            #                new_list+=" $new_dep"
            #                is_stop=false
            #            fi
            #        done
            #    fi
            #    elt_list="$new_dep $elt_list"
            #done
        done
        run_list="$(echo $elt_list)"
    done

    echo $run_list
}

####################################################################################################
# INIT_2: VARIABLES AND CONSTANTS DEFINITION
# ?) Args parsing and formating
IS_INIT=false
if [[ $* =~ 'initrootfs' ]]; then
    IS_INIT=true
    RUN_LIST=$(echo $* | sed 's/initrootfs//')
else
    RUN_LIST=${*:-"ALL"}
fi
IS_NO_DEPENDENCY=false
if [[ $* =~ '--no-dep' ]]; then
    IS_NO_DEPENDENCY=true
    RUN_LIST=$(echo $* | sed 's/--no-dep//')
fi

# Set constants 1
POD_ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
SCRIPT_ROOT="${POD_ROOT}/scripts"
INFRA_ROOT="${POD_ROOT}/infra"

# ?) Source the config and utils
source $POD_ROOT/config.sh
source $SCRIPT_ROOT/core/utils.sh

if [[ $IS_INIT == true ]]; then
    ROOTFS=$CFG_ROOTFS
else
    unset ROOTFS
fi

# ?) Setup log debug and print some info
DEBUG_LOG="/dev/null" # TO DISABLE LOGS, COMMENT NEXT LINE
DEBUG_LOG=.start.log
[[ $DEBUG_LOG != "/dev/null" ]] && rm -f $DEBUG_LOG && exec &> >(tee -a "$DEBUG_LOG" | sed '/^\+/d' )
info ">>>>>  ENTRY-POINT  <<<<<"
silent "USER:   $USER"
silent "HOST:   $(domainname -f)"
silent "root:   $([[ $UID == 0 ]] && echo 'yes' || echo 'no')"
silent "PID:    $$"
silent "PPID:   $PPID"
silent "PWD:    $POD_ROOT"
silent "ROOTFS: $ROOTFS"
silent "EXTRA:  $(cd $POD_ROOT && ls *-config.sh 2>/dev/null)"


####################################################################################################
# INIT_3: CHECK AND REORDER RUN_LIST
# ?) In infra, check if given containers exists
all_container_list=$(find ${INFRA_ROOT} -name "pod.env")
container_list=""
if [[ ${RUN_LIST} != "ALL" ]]; then
    for c_to_run in ${RUN_LIST}; do
        if [[ $all_container_list =~ $c_to_run/pod.env ]]; then
            container_list+="$c_to_run "
        else
            print_error $? "No infra container $c_to_run found - Abort"
            exit
        fi
    done
else
    container_list="ALL"
fi

# Update of the RUNLIST in case of bad container in args
RUN_LIST="${container_list}"
silent "TARGET: $RUN_LIST"

# ?) ALL parsing and get correct container setup order in RUN_LIST (numerical order)
_tmp=$(mktemp -q)
if [[ $RUN_LIST == "ALL" ]]; then
    for container in $(find ${INFRA_ROOT} -name "pod.env"); do
        _fullpath=$(dirname $container)
        echo ${_fullpath#"$INFRA_ROOT/"} >> $_tmp
    done
else
    for container in $RUN_LIST; do
        _fullpath=$(dirname $(find ${INFRA_ROOT} -wholename "*${container}/pod.env"))
        echo ${_fullpath#"$INFRA_ROOT/"} >> $_tmp
    done
fi
RUN_LIST=$(cat $_tmp | sort --version-sort)
rm $_tmp

# ?) If no dependency is required do not update RUN_LIST
if [[ $IS_NO_DEPENDENCY == false ]]; then
    RUN_LIST=$(get_dep_run_list $RUN_LIST)
    silent "DEPEND: $RUN_LIST"
    exit
fi

# Set constant 2
NETWORK_NAME="bobynet"
DOMAIN_NAME="$CFG_DOMAIN_NAME"
COMMON_ETC_HOSTS="${POD_ROOT}/.includes/.etc_hosts"


####################################################################################################
# MAIN_1: PODMAN INIT ACTIONS
# 1.3) Create main working directory, Load Container & Network
info "Init Network & Containers"
for container in $RUN_LIST; do
    c_name=$(get_podcfg $container CTN_NAME)
    c_workdir=$(get_podcfg $container CTN_WORKDIR)
    # Create working directory
    [[ -n $c_workdir ]] && mkdir -p "$CFG_LIVE_DATA/$c_workdir"
    # Remove previous instance
    podman rm -f $c_name &> /dev/null
done
is_network_created=false
if [[ -z "$(podman ps -q)" ]]; then
    podman network rm -f $NETWORK_NAME 1>> $DEBUG_LOG
    podman network create $NETWORK_NAME 1>> $DEBUG_LOG
    is_network_created=true
fi
if [[ -n $ROOTFS ]] && [[ -n $CFG_LIVE_DATA ]]; then
    mkdir -p $ROOTFS $CFG_LIVE_DATA
    sudo cp -a $POD_ROOT/$ROOTFS/* $CFG_LIVE_DATA &>> $DEBUG_LOG
    # TODO: to put in pod_pre_config
    mkdir -p \
        $CFG_LIVE_DATA/nginx/certs
fi

# DEPRECATED (Was needed (ubuntu 22).. but not now (Debian 12) : to investigate)
# 1.3.1) Patch network config to avoid podman warning and error
#if [[ $is_network_created ]]; then
#    if [[ $UID == 0 ]]; then
#        _config_file_to_patch="/etc/cni/net.d/$NETWORK_NAME.conflist";
#    else
#        _config_file_to_patch="$HOME/.config/cni/net.d/$NETWORK_NAME.conflist";
#    fi
#    warn "Due to CniVersion set to 1.0.0 by default, it is leading to an"
#    silent "\\t\\terror for podman plugins: bridge, portmap, firewall and tuning."
#    silent "\\t\\t[PATCH 1] set CniVersion=0.4.0 in $(basename $_config_file_to_patch)"
#    sed -i "s/1.0.0/0.4.0/" $_config_file_to_patch 1>> $DEBUG_LOG
#    silent "\\t\\t[PATCH 2] set domainName=$DOMAIN_NAME in $(basename $_config_file_to_patch)"
#    sed -i "s/dns.podman/$DOMAIN_NAME/g" $_config_file_to_patch 1>> $DEBUG_LOG
#fi

# 1.3.2) Pull needed image if necessary (e.i. if not already pulled)
# TODO: get image list from infra data, and depedency
for container in $RUN_LIST; do
    _img_list+="$(get_podcfg $container CTN_IMAGE) "
done
#_img_list="  mariadb:latest"
#_img_list+=" $WP_IMG_NAME $WPFCGI_IMG_NAME"
#_img_list+=" someguy123/net-tools"
#_img_list+=" nginx"
_err=0
for img in $_img_list; do
    if [[ -z $(podman image ls --noheading $img) ]] &>> $DEBUG_LOG; then
        if podman pull docker.io/$img; then
            success "$img pulled"
        else
            warn "Cannot pull docker.io $img image - Skip"
            _err=1
        fi
    fi
    print_error_exit $_err "Some image(s) are not available - Abort"
done
# 1.3.4) Dynamic variables setup and /etc/hosts gnerateration for all containers
NETWORK_IP=$(podman network inspect $NETWORK_NAME | grep 'subnet"' | sed 's/.* "//g' | sed 's/".*$//g') &>> $DEBUG_LOG
GATEWAY_IP=$(podman network inspect $NETWORK_NAME | grep 'gateway"' | sed 's/.* "//g' | sed 's/".*$//g') &>> $DEBUG_LOG
DNS_CTN_IP=${GATEWAY_IP%.*}.$((${GATEWAY_IP##*.}+1))
if [[ $is_network_created == true ]]; then
    notify "Common /etc/hosts generated: $DOMAIN_NAME -> $GATEWAY_IP(podman gtw)"
    echo "$GATEWAY_IP $DOMAIN_NAME www.$DOMAIN_NAME" > $COMMON_ETC_HOSTS
fi


####################################################################################################
# MAIN_2: SEQUENCER PRE_CONFIG/RUN/CONFIG/POST_CONFIG
for container in $RUN_LIST; do
    c_name=$(get_podcfg $container CTN_NAME)
    file=$(find $INFRA_ROOT -wholename "*${container}/pod.env")
    unset pod_pre_config
    [[ -f $file ]] && source $file
    if [[ $(type -t pod_pre_config) == function ]]; then
        info "PRE-SETUP ${c_name} container"
        pod_pre_config
        if ! print_error $? "Cannot pre_config container ${c_name} - Skip"; then
            # Remove the container from the RUN_LIST
            RUN_LIST=${RUN_LIST//"$container"/}
            silent "\t\tContainer $c_name remove from TARGETS to run"
        fi
    fi
done

for container in $RUN_LIST; do
    c_name=$(get_podcfg $container CTN_NAME)
    file=$(find $INFRA_ROOT -wholename "*${container}/pod.env")
    unset pod_run pod_config
    if [[ -f $file ]]; then
        source $file
        podman container rm -f $c_name &>> $DEBUG_LOG
        info "RUN ${c_name} container"
        pod_run
        if print_error $? "Cannot run container ${c_name} - Skip"; then
            if [[ $(type -t pod_config) == function ]]; then
                info "CONFIG ${c_name} container"
                pod_config
            fi
        else
            # Remove the container from the RUN_LIST
            RUN_LIST=${RUN_LIST//"$container"/}
            silent "\t\tContainer $c_name remove from TARGETS to run"
        fi
    fi
done

# Fetch all remote proccessus, wait until they end properly.
if [[ -n ${PID_LIST[@]} ]]; then
    info "Wait for all remote config jobs to finish"
    silent "\t\tPIDs: ${PID_LIST[@]}"
    wait ${PID_LIST[@]}
    PID_LIST=()
fi

for container in $RUN_LIST; do
    c_name=$(get_podcfg $container CTN_NAME)
    file=$(find $INFRA_ROOT -wholename "*${container}/pod.env")
    unset pod_post_config
    [[ -f $file ]] && source $file
    if [[ $(type -t pod_post_config) == function ]]; then
        info "POST-CONFIG ${c_name} container"
        pod_post_config
    fi
done

# Fetch all remote proccessus, wait until they end properly.
if [[ -n ${PID_LIST[@]} ]]; then
    info "Wait for all remote config jobs to finish"
    silent "\t\tPIDs: ${PID_LIST[@]}"
    wait ${PID_LIST[@]}
fi

# If script goes here, means setup is fine. Print success message and quit.
success "$(basename $POD_ROOT) started !"
