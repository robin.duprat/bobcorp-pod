#!/bin/bash -i
####################################################################################################
# Note: Use interactive to get bashrc loaded, and so info/debug/error logger functions too.
#       if logger functions are not loaded, wrap them to avoid meaningless errors
for fct in silent notify info warn error success; do
    if ! [[ $(type -t $fct) =~ "function" ]] &>/dev/null; then eval "$fct()" '{ echo ---- $*; }'; fi
done

####################################################################################################
# 0) Log debug setup
#DEBUG_LOG="/dev/null"
DEBUG_LOG=.backup.log
FOLDER_LIST="$(echo $(find live/ -maxdepth 1 -type d | sed '/\.\/\..*/d' | sed '/^\.$/d'))"
[[ $DEBUG_LOG != "/dev/null" ]] && rm -f $DEBUG_LOG && exec &> >(tee -a "$DEBUG_LOG" | sed '/^\+/d' )
info ">>>>>  ENTRY-POINT  <<<<<"
silent "USER:   $USER"
silent "HOST:   $(domainname -f)"
silent "root:   $([[ $UID == 0 ]] && echo 'yes' || echo 'no')"
silent "PID:    $$"
silent "PPID:   $PPID"
silent "PWD:    $PWD"
silent "TARGET: $FOLDER_LIST"
set -x

####################################################################################################
# 1.1) create main working directory
info "Init backup compression"
BACKUP_DIRNAME=".backup"
mkdir -p $BACKUP_DIRNAME &>/dev/null
NAME=${1:-"save_$(date +'%Y-%m-%d %H:%M:%S' | sed 's/ /-/g')"}
EXT_ZIP='zip'
EXT_TAR='tar'
EXT=$EXT_TAR
# DEPRECATED EXT=$EXT_ZIP;
CMD_ZIP="zip -r $BACKUP_DIRNAME/$NAME.$EXT $FOLDER_LIST"
CMD_TAR="tar -cf $BACKUP_DIRNAME/$NAME.$EXT $FOLDER_LIST"
CMD=$CMD_TAR
# DEPRECATED CMD=CMD_ZIP
( sudo $CMD ) &>> $DEBUG_LOG
[[ $? != 0 ]] && warn "Backup of $NAME not ended properly"
test -f $BACKUP_DIRNAME/$NAME.$EXT && success "Backup generated: $BACKUP_DIRNAME/$NAME.$EXT"
