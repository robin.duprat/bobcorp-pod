#!/bin/bash -i
####################################################################################################
# Note: Use interactive to get bashrc loaded, and so info/debug/error logger functions too.
#       if logger functions are not loaded, wrap them to avoid meaningless errors
for fct in silent notify info warn error success; do
    if ! [[ $(type -t $fct) =~ "function" ]] &>/dev/null; then eval "$fct()" '{ echo ---- $*; }'; fi
done

####################################################################################################
source config.sh
# 0) Log debug setup
DEBUG_LOG="/dev/null" # TO DISABLE LOGS, COMMENT NEXT LINE
DEBUG_LOG="$PWD/.clone.log"
SRV_NAME="$CFG_SRV_FTP_HOST"
DB_SAVE="$(readlink -f $1 2>/dev/null)"
[[ $DEBUG_LOG != "/dev/null" ]] && rm -f $DEBUG_LOG && exec &> >(tee -a "$DEBUG_LOG" | sed '/^\+/d' )
info ">>>>>  ENTRY-POINT  <<<<<"
silent "USER:   $USER"
silent "HOST:   $(domainname -f)"
silent "root:   $([[ $UID == 0 ]] && echo 'yes' || echo 'no')"
silent "PID:    $$"
silent "PPID:   $PPID"
silent "PWD:    $PWD"
silent "SERVER: $SRV_NAME"
silent "DBSave: $DB_SAVE"
set -x

####################################################################################################
SRV_USER="$CFG_SRV_FTP_USER"
SRV_PASS="$CFG_SRV_FTP_PASS"
ROOTFS=$CFG_ROOTFS

# 1.1) FTP html/ download
info "Init FTP cloning from $SRV_NAME"
mkdir -p $ROOTFS; cd $ROOTFS
wget -m --user=$SRV_USER --password=$SRV_PASS ftp://$SRV_NAME &>> $DEBUG_LOG && \
success "FTP connexion ends successfully" || \
( warn "Bad FTP clone, may check config.sh and see $DEBUG_LOG" && exit 1 )
sudo rm html/* -rf; mv $SRV_NAME/www html && rm $SRV_NAME -r && \
success "$SRV_NAME/www successfully cloned to $ROOTFS/html"

info "Get wordpress UID:GUID to apply"
TMP_DIR=$(mktemp -d)
podman container rm -f wp_temp &>> $DEBUG_LOG
podman run --name wp_temp -v "$TMP_DIR":/var/www/html -d --rm docker.io/wordpress &>> $DEBUG_LOG || \
( error "Cannot launch container wordpress - Abort"; exit 1 )
_file="$TMP_DIR/.htaccess"; while ! test -f $_file; do continue; done && \
ID="$(stat -c '%u' $_file):$(stat -c '%g' $_file)"; sudo chown -R $ID html && \
podman container rm -f wp_temp &>> $DEBUG_LOG && \
success "Rights correctly applied on html/"


# 1.2) Load Database from dump
[[ $DB_SAVE == "" ]] && exit 0
DB_NAME="$CFG_DB_NAME"
DB_USER="$CFG_DB_USER"
DB_PASS="$CFG_DB_PASS"
SRV_HOST="$CFG_SRV_DB_HOST"
#SRV_USER="$CFG_SRV_DB_USER"
#SRV_PASS="$CFG_SRV_DB_PASS"
#SRV_DB_NAME="$CFG_SRV_DB_NAME"

mkdir -p database; sudo rm -rf database/*
info "Init Database cloning from $SRV_HOST"
podman container rm -f db_temp &> /dev/null
podman run --name db_temp -v "$PWD/database":/var/lib/mysql -v "$DB_SAVE":/tmp/save.sql -d \
           -e MARIADB_USER=$DB_USER \
           -e MARIADB_PASSWORD=$DB_PASS \
           -e MARIADB_ROOT_PASSWORD=$DB_PASS \
           -e MARIADB_DATABASE=$DB_NAME \
           --expose 3306 \
       docker.io/mariadb:latest  &>> $DEBUG_LOG || \
( error "Cannot launch container database - Abort"; exit 1 )
info "Configure Database cloning from $SRV_HOST"
sleep 8 && podman container exec db_temp bash -c \
    "cat /tmp/save.sql | mariadb --user="$DB_USER" --password="$DB_PASS" $DB_NAME"  ||
( error "Cannot dump from $DB_SAVE"; echo podman container rm -f db_temp &> /dev/null; exit 1 ) && \
success "Database dump done"; #podman container rm -f db_temp &>> $DEBUG_LOG

# End
cd -
