#!/bin/bash -i
####################################################################################################
# Note: Use interactive to get bashrc loaded, and so info/debug/error logger functions too.
#       if logger functions are not loaded, wrap them to avoid meaningless errors
for fct in silent notify info warn error success; do
    if ! [[ $(type -t $fct) =~ "function" ]] &>/dev/null; then eval "$fct()" '{ echo ---- $*; }'; fi
done

####################################################################################################
# 0) Log debug setup
DEBUG_LOG="/dev/null" # TO DISABLE LOGS, COMMENT NEXT LINE
DEBUG_LOG=.upload.log
[[ $DEBUG_LOG != "/dev/null" ]] && rm -f $DEBUG_LOG && exec &> >(tee -a "$DEBUG_LOG" | sed '/^\+/d' )
info ">>>>>  ENTRY-POINT  <<<<<"
silent "USER:   $USER"
silent "HOST:   $(domainname -f)"
silent "root:   $([[ $UID == 0 ]] && echo 'yes' || echo 'no')"
silent "PID:    $$"
silent "PPID:   $PPID"
silent "PWD:    $PWD"
set -x


####################################################################################################
# 1) Setup environment
source config.sh
# 1.1) HTML upload
info "Init Upload"
_err=0
SRV_HOST="$CFG_SRV_FTP_HOST"
SRV_USER="$CFG_SRV_FTP_USER"
SRV_PASS="$CFG_SRV_FTP_PASS"
START_FLAG="$CFG_EXTRA_CONFIG_START_FLAG"
END_FLAG="$CFG_EXTRA_CONFIG_END_FLAG"
DOMAIN_NAME="$CFG_DOMAIN_NAME"
info "Remove extra config"
for file in $(grep -lr "$START_FLAG" html/); do
    START_LINE_NUM=$(( grep -nh "$START_FLAG" $file ) | sed 's/:.*//g')
    END_LINE_NUM=$(( grep -nh "$END_FLAG" $file ) | sed 's/:.*//g')
    _start=""
    _end=""
    while [[ $START_LINE_NUM != $_start ]] && [[ $END_LINE_NUM != $_end ]]; do
        _start=${START_LINE_NUM%% *}
        _end=${END_LINE_NUM%% *}
        if [[ $_start -ge $_end ]]; then
            warn "BAD CONFIG REMOVAL - Skip $file($_start:$_end)"
            _err=1
            break
        else
            sudo sed -i "${_start},${_end}d" $file
            notify "Remove config: $file($_start:$_end)"
        fi
        START_LINE_NUM=${START_LINE_NUM#* }
        END_LINE_NUM=${END_LINE_NUM#* }
    done
done

[[ $_err != 0 ]] && error "Exit due to previous error(s)" && exit $_err
success "Successfully remove extra config"
wdel ftp://$SRV_USER:$SRV_PASS@$SRV_HOST:21:/www/ ||
wput --basename="html/" html/* ftp://$SRV_USER:$SRV_PASS@$SRV_HOST:21:/www/ ||
error "Cannot upload some files, see $DEBUG_LOG." && \
success "Successfully upload all http/ files"

# 1.2) Database Dump
info "Init Database dump"
_container=mariadb
DOMAIN_NAME=$CFG_DOMAIN_NAME
DB_NAME=$CFG_DB_NAME
DB_USER=$CFG_DB_USER
DB_PASS=$CFG_DB_PASS
if [[ "$(podman container inspect -f '{{.State.Running}}' $_container)" != "true" ]]; then
    _container=db_temp
    podman run --name $_container -v "$PWD/database":/var/lib/mysql -v -d \
               -e MARIADB_USER=$DB_USER \
               -e MARIADB_PASSWORD=$DB_PASS \
               -e MARIADB_ROOT_PASSWORD=$DB_PASS \
               -e MARIADB_DATABASE=$DB_NAME \
           docker.io/mariadb:latest  &>> $DEBUG_LOG || \
    ( error "Cannot launch container database - Abort"; exit 1 )
    sleep 8
fi
podman container exec $_container bash -c \
    "mariadb-dump --user='$DB_USER' --password='$DB_PASS' '$DB_NAME' > /var/lib/mysql/dump_$DOMAIN_NAME.sql" || \
( error "Cannot dump from $DB_SAVE"; [[ $_container == "db_temp" ]] && podman container rm -f db_temp &> /dev/null; exit 1 ) && \
success "Database dump done";
[[ $_container == "db_temp" ]] && podman container rm -f db_temp || true
