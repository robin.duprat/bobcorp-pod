# BobCorp Pod (Be Out Business Corporation Podman)

Pay Nothing, Share Everything.

Easy podman rootless containers setup for WEB micro-service development.


# Prerequise
You only need to have [podman](https://podman.io/) installed.


# Start
To start following (published) containers :
- Wordpress: localhost:8080(HTTP) / localhost:8443(HTTPS)
- iRedMail (with roundcube) : localhost:8082
- Jenkins : localhost:8083(HTTP/HTTPS)

Run :
```
$ ./start.sh
```

A reverse proxy, toolbox (for jenkins agent) and private DNS containers will be running too,
ensuring network config and offering test tools.

Note:
1) DNS not correctly setup yet
2) Jenkins HTTPS is not auto-configured (in opposition with wordpress container), but it
   can be once you generate a ./jenkins/jenkins.jks (certificate) with 'password' password.
   To do it so, uncomment .include/tools/generate_certs.sh line for keytools and run the
   script with "jenkins.bobcorp.net"


# Backup

If you want to backup your micro-services rootfs (work volumes), run :
```
$ ./backup.sh <name_of_your_backup>
```
And a tar will be create (with all your volumes saved, except network configuration in
.includes/volumes, that stay dynamic)


# Restore

To restore a backup, simply run:
```
$ ./restore.sh <path_of_your_backup>
```

# Additionals

To get full integrated bobcorp-pod applications on your host web browser, add this in
your local /etc/hosts file to add a DNS entry for podman domain (bobcorp.net by default) :
```
127.0.0.1   bobcorp.net   www.bobcorp.net
```

Then you will be able to access all micro-services via www.bobcorp.net:<PORT_OF_APP> (see [*start*](#start) section for port mapping)

# Todo

Some cleanup :
1) Split in several bash files the run / setup definition of a group of container (ex: wordpress+mariadb, strapi+mariadb, etc..)
2) Add a 'pre' and 'post' configuration for container with priority (to execute the extra-setup job in the right order/step with some podman restart need)

Some improvement :
1) do not use podman restart in script + Add in dedicated restart function the update of container IP in DNS setup, with dns restart may be (need to have the same DNS address, because it have been given at container run as DNS to used)

# Multiarch compatibility with qemu
See:
- from x86\_64: https://github.com/multiarch/qemu-user-static
- from other: https://dbhi.github.io/qus/#
