#! /bin/bash

if [ "$#" -ne 1 ]
then
  echo "Error: No domain name argument provided"
  echo "Usage: Provide a domain name as an argument"
  exit 1
fi

# --- args ---
DOMAIN=$1
OUT=${1%%.*}
# --- var ----
IP="127.0.0.1"

mkdir -p $DOMAIN; rm -r $DOMAIN/*; cd $DOMAIN

# Create root CA & Private key
openssl req -x509 \
            -sha256 -days 356 \
            -nodes \
            -newkey rsa:2048 \
            -subj "/CN=${DOMAIN}/C=FR/L=Ardeche(07)" \
            -keyout $OUT-rootCA.key -out $OUT-rootCA.crt

# Generate Private key
openssl genrsa -out ${DOMAIN}.key 2048

# Create csf conf
cat > csr.conf <<EOF
[ req ]
default_bits = 2048
prompt = no
default_md = sha256
req_extensions = req_ext
distinguished_name = dn

[ dn ]
C = FR
ST = Ardeche(07)
L = Bobyland
O = Be Out Business Corporation
OU = N/A
CN = www.${DOMAIN}

[ req_ext ]
subjectAltName = @alt_names

[ alt_names ]
DNS.1 = ${DOMAIN}
IP.1 = $IP
DNS.1 = www.${DOMAIN}
IP.1 = $IP
EOF

# create CSR request using private key
openssl req -new -key ${DOMAIN}.key -out ${DOMAIN}.csr -config csr.conf

# Create a external config file for the certificate
cat > cert.conf <<EOF

authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
subjectAltName = @alt_names

[alt_names]
DNS.1 = ${DOMAIN}

EOF

# Create SSl with self signed CA
openssl x509 -req \
    -in ${DOMAIN}.csr \
    -CA $OUT-rootCA.crt -CAkey $OUT-rootCA.key \
    -CAcreateserial -out ${DOMAIN}.crt \
    -days 365 \
    -sha256 -extfile cert.conf

# Create keystore
openssl pkcs12 -export -out ${DOMAIN}.p12 -passout 'pass:password' -inkey $OUT-rootCA.key -in $OUT-rootCA.crt -name ${DOMAIN}
#DEPRECATED since jenkins https will come from nginx (reverse proxy)
#keytool -importkeystore -srckeystore ./${DOMAIN}.p12 -srcstoretype pkcs12 -destkeystore $OUT.jks -deststoretype JKS

# Generate Other format file/links
ln -sf ${DOMAIN}.key privkey.pem
cat ${DOMAIN}.crt $OUT-rootCA.crt > fullchain.pem
